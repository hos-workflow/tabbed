static const char* normbgcolor  = "#282A2E";
static const char* normfgcolor  = "#707880";
static const char* selbgcolor   = "#1D1F21";
static const char* selfgcolor   = "#C5C8C6";
static const char* urgbgcolor   = "#111111";
static const char* urgfgcolor   = "#cc0000";
