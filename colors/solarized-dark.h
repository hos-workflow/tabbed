static const char* urgbgcolor   = "#d01b24";
static const char* urgfgcolor   = "#000000";
static const char* selbgcolor   = "#728905";
static const char* selfgcolor   = "#000000";
static const char* normbgcolor  = "#001e26";
static const char* normfgcolor  = "#e9e2cb";
